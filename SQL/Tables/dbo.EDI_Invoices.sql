USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EDI_Invoices](
	[EDIInvoiceSK] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[SOPNumber] [char](21) NULL,
	[Status] [nvarchar](20) NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[PostedDate] [datetime] NULL,
	[InvoiceData] [xml] NOT NULL,
	[ExportedInvoice] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

