USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EDI_Setup](
	[Company] [nvarchar](50) NOT NULL,
	[InboundFolder] [nvarchar](255) NOT NULL,
	[ArchiveFolder] [nvarchar](255) NOT NULL,
	[OutboundFolder] [nvarchar](255) NOT NULL,
	[FailureFolder] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO

