USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[NATFO_GetCustomerLookup](@CustomerNumber VARCHAR(15))
RETURNS @returnTable TABLE
(
	[Priority] INT PRIMARY KEY NOT NULL,
	[EDICustomerSK] BIGINT NOT NULL
)
AS
BEGIN
	;WITH CustomerChain ([Priority], [EDICustomerSK])  AS
	(
		--Direct customer is highest priority
		SELECT	 0
				,u.EDICustomerSK
		FROM [NATFO].dbo.RM00101 c
		JOIN EDI_Tracking.dbo.EDI_Customers u
			ON u.CustomerNumber = c.CUSTNMBR
		WHERE c.CUSTNMBR = @CustomerNumber
		AND u.IsLive = 1

		UNION

		--Then comes corporate customer name
		SELECT	 1
				,u.EDICustomerSK
		FROM [NATFO].dbo.RM00101 c
		JOIN [NATFO].dbo.RM00105 m
			ON m.CPRCSTNM = c.CPRCSTNM
		JOIN EDI_Tracking.dbo.EDI_Customers u
			ON u.CustomerNumber = m.CPRCSTNM
		LEFT JOIN EDI_Tracking.dbo.EDI_Customers r
			ON r.CustomerNumber = @CustomerNumber
		WHERE c.CUSTNMBR = @CustomerNumber
		AND u.IsLive = 1
		AND ISNULL(r.IsLive, 1) <> 0 --Only when customer is literally defined as not live do we ignore its National Account
	)
	INSERT @returnTable
	SELECT c.[Priority]
		 , c.EDICustomerSK
	FROM CustomerChain c

	RETURN
END
GO

