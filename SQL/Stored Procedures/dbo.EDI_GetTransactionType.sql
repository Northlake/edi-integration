USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[EDI_GetTransactionType]
	@CompanyName VARCHAR(5),
	@CustomerName VARCHAR(15),
	@IncomingType INT
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX) = ''
	SET @SQL = @SQL + '
	;WITH TransactionTypeReference AS
	(
		SELECT	 ROW_NUMBER() OVER (ORDER BY l.[Priority]) ''RowID''
				,l.[Priority]
				,t.IncomingType
				,t.OutgoingType
		FROM dbo.' + @CompanyName + '_GetCustomerLookup(''' + @CustomerName + ''') l
		JOIN dbo.EDI_TransactionTypeReference t
			ON t.EDICustomerSK = l.EDICustomerSK
	)
	SELECT	t.OutgoingType
	FROM TransactionTypeReference t
	WHERE t.RowID = 1 --Take only lowest priority
	AND t.IncomingType = ' + CONVERT(NVARCHAR(20), @IncomingType)

	EXEC (@SQL)
END


GO

