USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EDI_GetUnits]
	@CompanyName VARCHAR(5),
	@CustomerName VARCHAR(15)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX) = ''
	SET @SQL = @SQL + '
	;WITH UnitReference AS
	(
		SELECT	 ROW_NUMBER() OVER (PARTITION BY r.ItemNumber ORDER BY l.[Priority]) ''RowID''
				,l.[Priority]
				,r.ItemNumber
				,r.UnitToUse
		FROM dbo.' + @CompanyName + '_GetCustomerLookup(''' + @CustomerName + ''') l
		JOIN dbo.EDI_UOMReference r
			ON r.EDICustomerSK = l.EDICustomerSK
	)
	SELECT	 u.ItemNumber
			,u.UnitToUse 
	FROM UnitReference u
	WHERE u.RowID = 1 --Take only lowest priority'

	EXEC (@SQL)
END
GO

