USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EDI_GetSite]
	@CompanyName VARCHAR(5),
	@CustomerName VARCHAR(15)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX) = ''
	SET @SQL = @SQL + '
	;WITH SiteReference AS
	(
		SELECT	 ROW_NUMBER() OVER (ORDER BY l.[Priority]) ''RowID''
				,l.[Priority]
				,s.SiteID
		FROM dbo.' + @CompanyName + '_GetCustomerLookup(''' + @CustomerName + ''') l
		JOIN dbo.EDI_SiteReference s
			ON s.EDICustomerSK = l.EDICustomerSK
	)
	SELECT	 s.SiteID
	FROM SiteReference s
	WHERE s.RowID = 1 --Take only lowest priority'

	EXEC (@SQL)
END


GO

