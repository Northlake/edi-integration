USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EDI_GetInvoicesToProcess]
AS
	BEGIN
		SET NOCOUNT ON;

		SELECT
			i.EDIInvoiceSK
		  , i.InvoiceData
		  , i.[FileName]
		  FROM
			EDI_Invoices i
		  WHERE
			i.[Status] = 'New';
	END;
GO
