USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EDI_CustomerCheck]
	@CompanyName VARCHAR(5),
	@CustomerName VARCHAR(15)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX) = ''
	SET @SQL = @SQL + 'SELECT COUNT(*) FROM dbo.' + @CompanyName + '_GetCustomerLookup(''' + @CustomerName + ''')'

	EXEC (@SQL)
END
GO

