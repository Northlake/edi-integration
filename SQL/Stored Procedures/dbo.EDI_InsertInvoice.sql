USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EDI_InsertInvoice] 
	@filePath NVARCHAR(255),
	@contents xml
AS
	BEGIN
		SET NOCOUNT ON;

		INSERT EDI_Invoices ([FileName],[Status],InsertDate,InvoiceData)
		VALUES (@filePath,'New',GETUTCDATE(),@contents)
	END
GO

