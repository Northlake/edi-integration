USE [EDI_Tracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EDI_LogMessage] 
	@Message nvarchar(max)
AS
	BEGIN
		SET NOCOUNT ON;

		INSERT EDI_Logging (LoggedTime,[Message])
		VALUES (GETUTCDATE(), @Message)
	END
GO

